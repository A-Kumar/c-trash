#include <stdio.h>

struct dob {
	int day, moth, year;
};

struct student {
	char* name;
	int roll;
	struct dob date;
};

int main() {
	
	struct student s = {"student1", 1, {1,1,1990}};
	struct student s1 = {"student2", 2, {1,2,1992}};
	
	struct student *p[2];
	p[0] = &s;
	p[1] = &s1;
	int i;
	for(i = 0; i < 2;  i++){
		printf("\n\nstudent name: %s \nroll no. %d \nDate of birth: %2d-%2d-%4d", p[i]->name, p[i]->roll, p[i]->date.day, p[i]->date.moth, p[i]->date.year);
	}
	
	return 0;
}
