#include <stdio.h>
#include <stdlib.h>

// making a node structure here

struct node {
	struct node* first;
	unsigned int data;
	struct node* last;
};


struct node* traverse_to_first(struct node* sample){
	while(sample->first){
		sample = sample->first;
	}
	return sample;
}

struct node* traverse_to_last(struct node* sample){
	while(sample->last){
		sample = sample->last;
	}
	return sample;
}

struct node* first_to_position(struct node* sample, unsigned int position){
	sample = traverse_to_first(sample);
	int i;
	for(i = 1; i < position - 1; i++){
		if(sample->last) {
			sample = sample->last;	
		}else{
			sample = NULL;
			printf("\n Position doesn't exist !!\n");
			break;
		}
	}
	return sample;
}

// creates an empty node
struct node* create_node (unsigned int data){
	struct node *temp = (struct node*)malloc(sizeof(struct node));
	
	// say if the os doesn't gives us the space..
	if(!temp){
		printf("Error !! cannot create a new node!");
		return NULL;
	}
	temp->first = NULL;
	temp->last = NULL;
	temp->data = data;
	return temp;
}

struct node* insert_start(struct node* sample, unsigned int data){

	// check if the node we got is the first node, if not, go to it
	sample = traverse_to_first(sample);

	struct node *temp =  create_node(data);
	temp->last = sample;
	sample->first = temp;

	// returns the added node
	return temp;
}

struct node* insert_last(struct node *sample, unsigned int data){
	// check if the node we got is the last node or not if not go there

	sample =  traverse_to_last(sample);

	struct node * temp = create_node(data);
	temp->first = sample;
	sample->last = temp;

	// returns the last node
	return temp;
}

struct node* delete_last(struct node* sample){
	sample = traverse_to_last(sample);
	sample = sample->first;
	free(sample->last);
	sample->last = NULL;
	return sample;
}

struct node* delete_first(struct node* sample){
	sample = traverse_to_first(sample);
	sample = sample->last;
	free(sample->first);
	sample->first = NULL;
	return sample;
}

struct node* delete_position(struct node* sample, unsigned int position){
	sample = first_to_position(sample, position);
	struct node *temp = sample->last;
	sample->last = temp->last;
	sample->last->first = sample;
	free(temp);
	return sample;
}

struct node* insert_position(struct node* sample, unsigned int data, unsigned int position){
	sample = first_to_position(sample, position);
	struct node *temp;
	if((sample->first == NULL) && (position == 1)){
		temp = insert_start(sample, data);
	}
	else if(sample->last == NULL){
		temp = insert_last(sample, data);
	}else {
		sample = first_to_position(sample, position);
		temp = create_node(data);
		temp->first = sample;
		temp->last = sample->last;
		sample->last = temp;
	}
	return temp;
}


void print_all_data(struct node* sample){
	struct node* traverse = traverse_to_first(sample);

	printf("\nTraversed data:\n");
	while(traverse){
		printf("\n%d\n", traverse->data);
		traverse = traverse->last;		  	
	}
}

int main(){
	struct node  *new_node = create_node(10);
	printf("Data in new node is %d \n", new_node->data);
	
	// insert a node in the beginning with data = 20
	struct node* sample1 = insert_start(new_node, 20);
	struct node* sample2 = insert_last(sample1, 30);
	struct node* sample3 = insert_position(sample2, 40, 3);
	struct node* sample4 = insert_last(new_node, 60);
	struct node* sample5 = insert_last(new_node, 334);
	struct node* sample9 = insert_last(new_node, 445);
	struct node* sample10 = insert_last(new_node, 232);

	print_all_data(sample1);

	struct node* sample6 = delete_last(new_node);
	struct node* sample7 = delete_first(new_node);
	struct node* sample8 = delete_position(sample7, 4 );
	print_all_data(new_node);


	// free the assigned memory
	free(new_node);
	free(sample1);
	free(sample2);
	free(sample3);
	free(sample4);
	free(sample5);
	free(sample6);
	free(sample7);
	free(sample8);
	free(sample9);
	free(sample10);


	return 0;
}
