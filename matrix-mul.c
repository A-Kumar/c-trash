#include <stdio.h>

int main(){

	int m = 2;
	int n = 3;
	int p = 4;

	// initialize arraiy
	int a[m][n], b[n][p], c[m][p];
	// int c[m][p] = {0};
	
	//initialize loop variables
	int i,j,k;

	for(i = 0; i < m; i++)
		for(k = 0; k < p; k ++)
			c[i][k] = 0;

	
	printf("\nInput for first array\n");
	// input first array
	for(i = 0; i < m; i++)
		for(j = 0; j < n; j++){
			printf("\nenter for positon %d %d \n", i,j);
			scanf("%d", &a[i][j]);
		}

	printf("\nInput for second array\n");
	// input second array
	for ( j  = 0; j < n; j++)
	       for(k = 0; k < p; k++) {
		       printf("\nenter for position %d %d \n", j, k);
	       		scanf("%d", &b[j][k]);
	       }
	// multiplication of arrays
	
	for(i = 0; i<m; i++)
		for(j = 0; j < n; j ++)
			for(k = 0; k < p; k++){
				c[i][k] += a[i][j] * b[j][k];
	}

	// print array c ~
	
	printf("\n");	
	for(i = 0; i<m; i++){
		printf("\n");
	       for(k = 0; k < p; k++)
	       		printf("%d ", c[i][k]);
	}
      	printf("\n");	
	return 0;
}
